#!/bin/bash
CONTAINER=ds
DISPLAY="$(hostname):0"
USER=xclient

docker run \
    -it \
    --rm \
    --user=$USER \
    -e DISPLAY \
    $CONTAINER \
    $1
