import preprocess as p
import pandas as pd
import gensim.models


COL_NUM = 13

df = pd.read_excel("data/CU1.xlsx").iloc[:, COL_NUM].dropna()
df = df.append(pd.read_csv("data/CU2.csv").iloc[:, COL_NUM]).dropna()

print(df)

documents = [p.gensim_preprocess(row) for row in df]

# print(documents)
model = gensim.models.Word2Vec(documents, size=30, window=3, min_count=1, workers=10)

model.train(documents, total_examples=len(documents), epochs=10)

print(model.wv.most_similar(positive="inventarios"))
print(model.wv["inventarios"])
