import networkx as nx
from typing import List, Dict


def is_subset(dict1, dict2):
    return dict1.items() <= dict2.items()


def common_keys_have_same_values(dict1, dict2):
    keys1 = set(dict1.keys())
    keys2 = set(dict2.keys())
    common_keys = keys1.intersection(keys2)
    for k in common_keys:
        if dict1[k] != dict2[k]:
            return False
    return True


def graphs(patterns: Dict[str, List[List[Dict[str, str]]]]) -> Dict[str, nx.DiGraph]:
    return {key: pattern_to_graph(pattern) for key, pattern in patterns.items()}


def pattern_to_graph(pattern: List[List[Dict[str, str]]]) -> nx.DiGraph:
    G = nx.DiGraph()

    for tuple in pattern:
        if len(tuple) >= 3 and (len(tuple) - 3) % 2 == 0:  # Tuple size must be 3 + 2n
            nodes = tuple[0::2]
            for n in nodes:
                if not G.has_node(n["as"]):
                    G.add_node(n["as"], **n)
                else:
                    if common_keys_have_same_values(n, G.nodes[n]):
                        # Adds values in n to existing node G.nodes[n]
                        for k, v in n.items():
                            G.nodes[n][k] = v
                    else:
                        raise ValueError(
                            "Inconsistent node definitions:\n%s\nvs\n%s" % n, G.nodes[n]
                        )

            edges = tuple[1::2]
            for j, e in enumerate(edges):
                print("Edge:", e)
                if isinstance(e, Dict):
                    # Since edges are interwoven with nodes in pattern, the nodes  are j and j+1
                    G.add_edge(nodes[j]["as"], nodes[j + 1]["as"], **e)
                else:
                    raise ValueError("Edge %s must be a dictionary" % e)
        else:
            raise ValueError("Tuple size must be 3+2n: %s" + tuple)
        return G
