from pattern import graphs
from typing import List, Dict


def patterns() -> List[List[Dict[str, str]]]:
    print("patterns")
    return graphs(
        {
            "passive verb": [
                [
                    {"pos": "PRON", "as": "PRON"},
                    {"dep": "obj"},
                    {"pos": "VERB", "as": "VERB"},
                ]
            ],
            "potential composite": [
                [
                    {"pos": "ADP", "as": "ADP"},
                    {"dep": "case"},
                    {"pos": "NOUN", "as": "NOUN2"},
                    {"dep": "case"},
                    {"pos": "NOUN", "as": "NOUN1"},
                ]
            ],
        }
    )
