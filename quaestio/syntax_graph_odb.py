import pyorient
import json
from orientdb.graph import Graph
from typing import List, AbstractSet
from uuid import uuid4


class Token:
    def __init__(self):
        self.pos_ = ""
        self.text = ""
        self.lemma_ = ""
        self.i = -1
        self.doc = -1


class SyntaxGraph:
    def __init__(self, odb_client):
        self.id = uuid4().hex
        print(self.id)
        self.odb_client = odb_client
        self.ROOT = (
            Token()
        )  # {"pos_": "ROOT", "text": "ROOT", "lemma": "ROOT", "i": -1}
        self.ROOT.pos_ = "ROOT"
        self.ROOT.text = "ROOT"
        self.ROOT.lemma_ = "ROOT"
        self.ROOT.i = -1
        self.root_id = -1
        self.ids = {}

    def clear(self):
        pass

    def get_unique_id(self, token):
        #     if must_be_unique(token):
        #         return token.text
        # else:
        # return hash((token.i, token.text, token.doc))
        return hash((token.i, token.doc))

    def add_token(self, token):
        cmd = (
            """ create vertex pos_%s set text=%s, lemma=%s, i="%s", tok_id="%s", order=sequence('tok_seq').next() """
            % (
                token.pos_,
                repr(token.text),
                repr(token.lemma_),
                token.i,
                self.get_unique_id(token),
            )
        )

        # print(cmd)
        return self.odb_client.command(cmd)

    def add_dep(self, token):
        cmd = (
            """ create edge dep_%s from %s to %s set order=sequence('dep_seq').next() """
            % (token.dep_, self.ids[token], self.ids[token.head])
        )
        # print(cmd)
        self.odb_client.command(cmd)

    def add_root_dep(self, token):
        cmd = (
            """ create edge dep_%s from %s to %s set order=sequence('dep_seq').next()"""
            % (token.dep_, self.ids[token], self.root_id)
        )
        # print("ROOT", cmd)
        self.odb_client.command(cmd)

    def add_docs(self, docs: List, excluded: AbstractSet[str] = {}):
        self.root_id = self.add_token(self.ROOT)[0]._rid

        for doc in docs:
            for token in doc:
                if token.pos_ not in excluded:
                    id = self.add_token(token)[0]._rid
                    self.ids[token] = id

        for doc in docs:
            for token in doc:
                if token.pos_ not in excluded and token.head.pos_ not in excluded:
                    if token.head == token:
                        self.add_root_dep(token)
                    else:
                        self.add_dep(token)

    def to_json(self):
        return {
            "root_id": self.root_id,
            "nodes": [
                {
                    "text": n.text,
                    "pos": n._class[4:],
                    "lemma": n.lemma,
                    "i": n.i,
                    "id": n._rid,
                }
                for n in self.odb_client.query("select from Token order by order", -1)
            ],
            "connections": [
                {
                    "id": e._rid,
                    "dep": e._class[4:],
                    "source": str(e._out),
                    "target": str(e._in),
                }
                for e in self.odb_client.query("select from Dep order by order", -1)
            ],
            "pos_types": [
                c.name[4:]
                for c in self.odb_client.query(
                    "select name from (select expand(classes) from metadata:schema ) where superClass='Token' order by name",
                    -1,
                )
            ],
        }
