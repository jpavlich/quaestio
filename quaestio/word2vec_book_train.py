import preprocess as p
import gensim.models


def read_book(filename):
    with open(filename, "r") as book_file:
        book_raw = book_file.read()
        book = p.text_book_preprocess(book_raw)
        print(len(book))
        return book


def train(books):
    corpus = []
    for bookname in books:
        corpus.extend(read_book("tmp/%s.txt" % bookname))

    print("Training")
    model = gensim.models.Word2Vec(corpus, size=300, window=5, min_count=2, workers=16)

    model.train(corpus, total_examples=len(corpus), epochs=10)
    model.save("data/c2_word2vec.model")


if __name__ == "__main__":
    train(["c1", "c2", "c3", "c4"])
