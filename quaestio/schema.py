#!/usr/bin/env python
import pyorient


def create_schema(db_name):
    client = pyorient.OrientDB("localhost", 2424)
    session_id = client.connect("root", "hola12345")

    try:
        client.db_drop(db_name)
    except:
        pass

    client.db_create(db_name, type=pyorient.DB_TYPE_GRAPH)

    client.db_open(db_name, "root", "hola12345")

    all_pos = [
        "ADJ",
        "ADP",
        "PUNCT",
        "ADV",
        "AUX",
        "SYM",
        "INTJ",
        "CONJ",
        "X",
        "NOUN",
        "DET",
        "PROPN",
        "NUM",
        "VERB",
        "PART",
        "PRON",
        "SCONJ",
    ]

    all_deps = [
        "acl",
        "advcl",
        "advmod",
        "amod",
        "appos",
        "aux",
        "auxpass",
        "case",
        "cc",
        "ccomp",
        "clf",
        "compound",
        "conj",
        "cop",
        "csubj",
        "csubjpass",
        "dep",
        "det",
        "discourse",
        "dislocated",
        "dobj",
        "expl",
        "fixed",
        "flat",
        "foreign",
        "goeswith",
        "iobj",
        "list",
        "mark",
        "mwe",
        "name",
        "neg",
        "nmod",
        "nsubj",
        "nsubjpass",
        "nummod",
        "obj",
        "obl",
        "orphan",
        "parataxis",
        "punct",
        "remnant",
        "reparandum",
        "root",
        "vocative",
        "xcomp",
    ]

    client.command("create class Token extends V")
    client.command("create property Token.text STRING")
    client.command("create property Token.lemma STRING")
    client.command("create property Token.i INTEGER")
    client.command("create property Token.order LONG")

    client.command("create index Token.order unique")

    client.command("create class pos_ROOT extends Token")

    client.command("CREATE SEQUENCE tok_seq TYPE ORDERED")
    client.command("CREATE SEQUENCE dep_seq TYPE ORDERED")

    for pos in all_pos:
        print(pos)
        client.command("create class pos_%s extends Token" % pos)

    client.command("create class Dep extends E")
    client.command("create property Dep.order LONG")

    client.command("create index Dep.order unique")

    for dep in all_deps:
        print(dep)
        client.command("create class dep_%s extends Dep" % dep)


if __name__ == "__main__":
    create_schema("quaestio")
