import networkx as nx
import json
from typing import List, AbstractSet
from uuid import uuid4
from networkx.readwrite import json_graph
from typing import List, Dict
from pattern_list import patterns
from networkx.algorithms.isomorphism import DiGraphMatcher

ROOT_NODE = "ROOT"

POS_COLOR = {
    "NOUN": "orange",
    "VERB": "yellow",
    "CONJ": "blue",
    "ADJ": "red",
    "DET": "gray",
    "NUM": "silver",
    "ADV": "olive",
    "ADP": "green",
    "INTJ": "maroon",
    "PRON": "aqua",
    "PUNCT": "black",
    "SYM": "lime",
    "X": "Teal",
    "PROPN": "purple",
    "PART": "fucsia",
    "SPACE": "navy",
    "SCONJ": "darkgray",
    "AUX": "beige",
    "POS": "tan",
    "ROOT": "brown",
}


class SyntaxGraph:
    def __init__(self):
        self.node_id = -1
        self.ids = {}
        self.patterns = patterns()
        self.G = nx.DiGraph()

    def get_unique_id(self, token):
        if not token in self.ids:
            self.node_id += 1
            self.ids[token] = self.node_id
            return self.node_id
        else:
            return self.ids[token]

    def add_docs(self, docs: List, excluded: AbstractSet[str] = {}):
        self.G = nx.DiGraph()
        self.G.add_node(self.get_unique_id(ROOT_NODE), text=ROOT_NODE, pos="ROOT")
        for doc in docs:
            for token in doc:
                self.G.add_node(
                    self.get_unique_id(token),
                    text=token.text,
                    pos=token.pos_,
                    lemma=token.lemma_,
                    i=token.i,
                    dep=token.dep_,
                )

        for doc in docs:
            for token in doc:

                if token.head == token:
                    self.G.add_edge(
                        self.get_unique_id(token), self.get_unique_id(ROOT_NODE)
                    )
                else:
                    self.G.add_edge(
                        self.get_unique_id(token),
                        self.get_unique_id(token.head),
                        dep=token.dep_,
                    )

    def match(self, rule: str):
        def equivalent(node1, node2):
            return node1["pos"] == node2["pos"]

        pattern = self.patterns[rule]

        GM = DiGraphMatcher(self.G, pattern, node_match=equivalent)
        for iso in GM.subgraph_isomorphisms_iter():
            print([(self.G.nodes[n1]["text"], n2) for n1, n2 in iso.items()])

    def to_json(self):
        attrs = {"link": "connections", "source": "source", "target": "target"}
        graph_data = json_graph.node_link_data(self.G, attrs)
        graph_data["root_id"] = self.get_unique_id(ROOT_NODE)
        graph_data["pos_types"] = list(POS_COLOR.keys())
        return graph_data
