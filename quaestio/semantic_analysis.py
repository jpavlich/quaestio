import pandas as pd
import spacy
from spacy.tokens import Token
import sys
import util
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
from functools import reduce
import io
from networkx.readwrite import json_graph

"""
Populates graph with info from syntax trees
"""

ROOT_NODE = "ROOT"


pos_color = {
    "NOUN": "orange",
    "VERB": "yellow",
    "CONJ": "blue",
    "ADJ": "red",
    "DET": "gray",
    "NUM": "silver",
    "ADV": "olive",
    "ADP": "green",
    "INTJ": "maroon",
    "PRON": "aqua",
    "PUNCT": "black",
    "SYM": "lime",
    "X": "Teal",
    "PROPN": "purple",
    "PART": "fucsia",
    "SPACE": "navy",
    "SCONJ": "darkgray",
    "AUX": "beige",
    "POS": "tan",
    "ROOT": "brown",
}


def accept(token):
    exclude_POS = [
        # 'DET',
        # 'PUNCT',
        # 'CONJ',
        # 'PRON'
    ]
    excluded = map(lambda pos: token.pos_ != pos, exclude_POS)

    return reduce(lambda x, y: x and y, excluded, True)


def must_be_unique(token):
    unique_POS = [
        # 'NOUN',
        # 'ADJ',
        # 'VERB',
    ]

    return token.pos_ in unique_POS
    # return token.pos_ == 'NOUN' or token.pos_ == 'ADJ' or token.pos == 'VERB'


def get_unique_id(token):
    if must_be_unique(token):
        return token.text
    else:
        return hash((token.i, token.text, token.doc))


def execute(docs):
    G = nx.DiGraph()
    G.add_node(ROOT_NODE, text=ROOT_NODE, pos="ROOT")
    for doc in docs:
        for token in doc:
            if accept(token):
                G.add_node(
                    get_unique_id(token),
                    text=token.text,
                    pos=token.pos_,
                    lemma=token.lemma_,
                    i=token.i,
                    dep=token.dep_,
                )

    for doc in docs:
        for token in doc:
            if accept(token) and accept(token.head):
                if token.head == token:
                    G.add_edge(get_unique_id(token), ROOT_NODE)
                else:
                    G.add_edge(
                        get_unique_id(token), get_unique_id(token.head), dep=token.dep_
                    )
    attrs = {"link": "connections", "source": "source", "target": "target"}
    graph_data = json_graph.node_link_data(G, attrs)
    graph_data["pos_types"] = list(pos_color.keys())
    return graph_data


def plot_graph(G):
    # See https://networkx.github.io/documentation/stable/reference/generated/networkx.drawing.nx_pylab.draw_networkx.html?highlight=draw_networkx#networkx.drawing.nx_pylab.draw_networkx
    pos = graphviz_layout(G, prog="dot", args="-Grankdir=BT")

    # Draw graph
    fig = plt.figure()
    ax = fig.add_subplot(111)

    labels_text = nx.get_node_attributes(G, "text")
    labels_pos = nx.get_node_attributes(G, "pos")
    node_labels = {}

    node_colors = [pos_color[pos] for _, pos in labels_pos.items()]
    nx.draw_networkx_nodes(G, pos, ax=ax, node_size=500, node_color=node_colors)
    for node in G.nodes:
        node_labels[node] = "%s\n%s" % (labels_text[node], labels_pos[node])

    nx.draw_networkx_edges(
        G, pos, ax=ax, edgelist=G.edges, edge_color="black", arrows=True
    )

    # edge_labels = nx.get_edge_attributes(G, 'dep')
    # nx.draw_networkx_edge_labels(
    #     G, pos, ax=ax, edge_labels=edge_labels, font_color='red')

    nx.draw_networkx_labels(G, pos, ax=ax, labels=node_labels)
    fig.tight_layout()
    # fig.axis('off')

    # Workaround for mpld3.fig_to_dict
    # out_json = io.StringIO()
    # mpld3.save_json(fig, out_json)
    # return out_json.getvalue()
    plt.savefig("tmp/fig.png")
    return "tmp/fig.png"


if __name__ == "__main__":
    execute(docs=util.load_docs("tmp/docs.bin"))
