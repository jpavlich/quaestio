from IPython.display import HTML, display
from tabulate import tabulate
import re
import os
import pickle
from spacy.tokens import Doc
import spacy
import sys

# Based on https://stackoverflow.com/a/43265713
def hex_color(rgba_color):
    red = int(rgba_color[0]*255)
    green = int(rgba_color[1]*255)
    blue = int(rgba_color[2]*255)
    return '#%02x%02x%02x' % (red, green, blue)

def print_docs(docs):
    for i, doc in enumerate(docs):
        print('\n' + str(i))
        for j, sent in enumerate(doc.sents):
            sys.stdout.write('\n\t' + str(j) + ': ')
            for tok in sent:
                # sys.stdout.write(str(tok.pos_) + ' ')
                sys.stdout.write('%s (%s) ' % (str(tok), tok.pos_))

# FIXME Not saving all elemens of the array


def save_docs(docs, filename):
    # See https://github.com/explosion/spaCy/issues/1258
    f = open(filename, 'wb')
    pickle.dump(list(docs), f)
    f.close()


def load_docs(filename):
    f = open(filename, 'rb')
    docs = pickle.load(f)
    f.close()
    return docs


def execute_all(client, multiline_string):
    multiline_string = re.sub(r"\n", " ", multiline_string, flags=re.UNICODE)
    multiline_string = re.sub(r"\s+", " ", multiline_string, flags=re.UNICODE)
    multiline_string = re.sub(r";", ";\n", multiline_string, flags=re.UNICODE)
    multiline_string = multiline_string.strip()
    for command in multiline_string.splitlines():
        cmd = command.strip()
        if (cmd != ''):
            try:
                client.execute(cmd)
            except Exception as e:
                print("Error: " + cmd + "\n" + str(e))
                return
        print(cmd)


def print_table(table):
    display(HTML(tabulate(table, tablefmt='html')))
