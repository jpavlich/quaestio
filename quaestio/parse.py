import re
import pandas as pd
import spacy
import sys
import util


def execute(corpus):
    nlp = spacy.load("es")
    # docs = nlp.pipe(corpus['text']) ## FIXME Resulting docs from this line does not serialize well
    docs = [nlp(doc) for doc in corpus]
    return docs


if __name__ == "__main__":
    docs = execute(pd.read_excel("tmp/corpus_processed.xlsx"))
    util.save_docs(docs, "tmp/docs.bin")
