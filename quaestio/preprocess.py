import re
import pandas as pd

# import gensim.utils as gu
# import gensim.parsing.preprocessing as gpp

# from pattern.es import lemma
from nltk.corpus import stopwords

# from pattern.es import parsetree

NUM_ELEMENTS_TO_PROCESS = 5
COL_NUM = 13


def preprocess(text):
    text = re.sub(r"\s+", " ", text, flags=re.UNICODE).strip().lower()
    text = re.sub(r"à", "á", text, flags=re.UNICODE)
    text = re.sub(r"è", "é", text, flags=re.UNICODE)
    text = re.sub(r"ì", "í", text, flags=re.UNICODE)
    text = re.sub(r"ò", "ó", text, flags=re.UNICODE)
    text = re.sub(r"ù", "ú", text, flags=re.UNICODE)
    text = re.sub(r"[()]", "", text, flags=re.UNICODE)
    return text
    # return gu.simple_preprocess(text, deacc=True)


def gensim_preprocess(text):
    return [
        word
        for word in text
        # gpp.preprocess_string(
        #     text,
        #     [
        #         lambda x: x.lower(),
        #         gpp.strip_tags,
        #         gpp.strip_punctuation,
        #         gpp.strip_multiple_whitespaces,
        #         gpp.split_alphanum,
        #         gpp.strip_numeric,
        #         gpp.strip_non_alphanum,
        #         # gpp.strip_short,
        #     ],
        # )
        if word not in get_stopwords()
    ]


def text_book_preprocess(text):
    text = re.sub(r"[\n\r\t]", " ", text, flags=re.UNICODE)
    # Substitutes punctuations to newlines
    text = re.sub(r"[.:;,-]", r"\n", text, flags=re.UNICODE)
    doc = [gensim_preprocess(sent) for sent in text.split("\n")]
    return [sent for sent in doc if len(sent) > 1]


def get_stopwords():
    sw = stopwords.words("spanish")
    return sw + ["si", "va"]


def execute(corpus_raw):
    corpus = corpus_raw.apply(preprocess).to_frame()
    corpus.columns = ["text"]
    return corpus


if __name__ == "__main__":
    corpus = execute(
        corpus_raw=pd.read_excel(r"data/CU1.xlsx").iloc[
            :NUM_ELEMENTS_TO_PROCESS, COL_NUM
        ]
    )
    corpus.to_excel("tmp/corpus_processed.xlsx")
