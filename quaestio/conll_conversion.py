import util
from pprint import pprint

# Based on https://github.com/explosion/spaCy/issues/533


def execute(docs):
    conll_docs = []
    for j, doc in enumerate(docs):
        conll_doc = []
        conll_docs.append(conll_doc)
        for k, sent in enumerate(doc.sents):
            print('doc %d.%d' % (j+1, k+1))
            conll_sent = []
            conll_doc.append(conll_sent)
            for i, word in enumerate(sent):
                if word.head is word:
                    head_idx = 0
                else:
                    head_idx = doc[i].head.i+1

                conll_sent.append("%d\t%s\t%s\t%s\t%s\t%s\t%d\t%s\t%s\t%s" % (
                    i+1,  # There's a word.i attr that's position in *doc*
                    word,
                    '_',
                    word.pos_,  # Coarse-grained tag
                    word.tag_,  # Fine-grained tag
                    '_',
                    head_idx,
                    word.dep_,  # Relation
                    '_', '_'))
            print('\n'.join(conll_sent))

    return conll_docs


if __name__ == '__main__':
    conll_sents = execute(
        docs=util.load_docs('tmp/docs.bin')
    )
