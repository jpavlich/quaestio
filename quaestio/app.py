import preprocess
import parse
import semantic_analysis
import conll_conversion
import pandas as pd
import re
import pyorient
from syntax_graph import SyntaxGraph
import json
from flask import Flask, request, jsonify
from flask_cors import CORS


app = Flask(__name__)
CORS(app)
syntax_graph = SyntaxGraph()


@app.route("/process", methods=["POST"])
def process():
    return json.dumps(analyze(request.json["text"])), 200


@app.route("/process-conll", methods=["POST"])
def process_conll():
    return json.dumps(analyze_conll(request.json["text"])), 200


def analyze(text):
    # corpus_raw = pd.DataFrame({"text": [text]}).iloc[:, 0]
    global syntax_graph

    corpus = preprocess.preprocess(text)  # .split(".")
    corpus = re.split(r"[ ]*[\.\n]+[ ]*", corpus)

    tree = parse.execute(corpus)
    syntax_graph.add_docs(docs=tree)
    syntax_graph.match("passive verb")
    return syntax_graph.to_json()


def analyze_conll(text):
    corpus_raw = pd.DataFrame({"text": [text]}).iloc[:, 0]
    corpus = preprocess.execute(corpus_raw)
    tree = parse.execute(corpus)

    return conll_conversion.execute(tree)

