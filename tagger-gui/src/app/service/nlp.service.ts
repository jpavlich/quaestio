import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NlpService {

  constructor(private http: HttpClient) { }

  process(text) {
    return this.http.post('http://localhost:5000/process-conll', { text: text });
  }
}
