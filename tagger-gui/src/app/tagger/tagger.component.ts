import { NlpService } from './../service/nlp.service';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-tagger',
  templateUrl: './tagger.component.html',
  styleUrls: ['./tagger.component.css']
})
export class TaggerComponent implements OnInit, AfterViewInit {
  editorOptions = { theme: 'hc-black' };
  text = 'el caso de uso comienza cuando el cliente llega al cajero con sus productos a comprar';
  docs: string[][];

  @ViewChild('canvas')
  canvasRef: ElementRef;

  canvas: any;


  constructor(private nlp: NlpService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.canvas = this.canvasRef.nativeElement;
  }

  updateCanvas() {
    this.nlp.process(this.text)
      .subscribe(
        (docs: string[][]) => {
          this.docs = docs;
          console.log(this.docs);
        },
        error => console.log(error));
  }

  onInit(editor) {

    // editor.onDidChangeCursorPosition((e) => {
    //   console.log(JSON.stringify(e));
    // });

    // editor.onDidChangeCursorSelection((e) => {
    //   console.log(JSON.stringify(e));
    // });

    const appComp = this;

    editor.addAction({
      // An unique identifier of the contributed action.
      id: 'process-text',

      // A label of the action that will be presented to the user.
      label: 'Process Text',

      // An optional array of keybindings for the action.
      keybindings: [
        // tslint:disable-next-line:no-bitwise
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.Enter,
      ],

      // A precondition for this action.
      precondition: null,

      // A rule to evaluate on top of the precondition in order to dispatch the keybindings.
      keybindingContext: null,

      contextMenuGroupId: 'navigation',

      contextMenuOrder: 1.5,

      // Method that will be executed when the action is triggered.
      // @param editor The editor instance is passed in as a convinience
      run: (ed) => {
        console.log('ctrl-enter pressed');
        // appComp.updateGraph();
        return null;
      }
    });
  }

}
