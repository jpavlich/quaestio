import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as d3 from 'd3';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  editorOptions = { theme: 'vs-dark' };
  code = '';
  result = '';

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    const svg = d3.select('svg'),
      width = +svg.attr('width'),
      height = +svg.attr('height');

    const color = d3.scaleOrdinal(d3.schemeCategory10);

    const simulation = d3.forceSimulation()
      .force('link', d3.forceLink().id(function (d: any) { return d.id; }))
      .force('charge', d3.forceManyBody())
      .force('center', d3.forceCenter(width / 2, height / 2));

    d3.json('assets/miserables.json')
      .then((graph: any) => {
        console.log(graph.links);
        const link = svg.append('g')
          .attr('class', 'links')
          .selectAll('line')
          .data(graph.links)
          .enter().append('line')
          .attr('stroke', 'black')
          .attr('stroke-opacity', '0.6')
          .attr('stroke-width', function (d: any) { return Math.sqrt(d.value); });

        const node = svg.append('g')
          .attr('class', 'nodes')
          .selectAll('circle')
          .data(graph.nodes)
          .enter().append('circle')
          .attr('r', 5)
          .attr('fill', function (d: any) { return color(d.group); })
          .call(d3.drag()
            .on('start', dragstarted)
            .on('drag', dragged)
            .on('end', dragended));

        node.append('title')
          .text(function (d: any) { return d.id; });

        simulation
          .nodes(graph.nodes)
          .on('tick', ticked);

        simulation.force<any>('link')
          .links(graph.links);

        function ticked() {
          link
            .attr('x1', function (d: any) { return d.source.x; })
            .attr('y1', function (d: any) { return d.source.y; })
            .attr('x2', function (d: any) { return d.target.x; })
            .attr('y2', function (d: any) { return d.target.y; });

          node
            .attr('cx', function (d: any) { return d.x; })
            .attr('cy', function (d: any) { return d.y; });
        }
      });

    function dragstarted(d) {
      if (!d3.event.active) { simulation.alphaTarget(0.3).restart(); }
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) { simulation.alphaTarget(0); }
      d.fx = null;
      d.fy = null;
    }

  }

  onInit(editor) {

    // editor.onDidChangeCursorPosition((e) => {
    //   console.log(JSON.stringify(e));
    // });

    // editor.onDidChangeCursorSelection((e) => {
    //   console.log(JSON.stringify(e));
    // });
  }

  process() {
    // this.http.post('http://localhost:5000/process', { 'text': 'el perro se comió la tarea' }, { responseType: ResponseContentType.Blob }))
    //   .subscribe(res : Response => this.updateGraph(res.blob()));
  }

}
