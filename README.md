# Requirements

* Python 3.6
* Conda


# Installation

* Create Conda environment
```
conda env create -f environment.yml
```
* Install spacy languages
```
python -m spacy download es
```
