import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphViewComponent } from './graph-view/graph-view.component';

import { MonacoEditorModule } from 'ngx-monaco-editor';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TreeViewComponent } from './tree-view/tree-view.component';


@NgModule({
  declarations: [
    AppComponent,
    GraphViewComponent,
    TreeViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MonacoEditorModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
