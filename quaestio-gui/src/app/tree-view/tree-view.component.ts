import { GraphService } from './../service/graph.service';
import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import { Graph } from '../model/graph';
import { TreeNode } from '../model/tree-node';
import { D3Service } from '../service/d3.service';
import { D3treeService } from '../service/d3tree.service';
import { selection } from 'd3';


// See https://bl.ocks.org/d3noob/08ecb6ea9bb68ba0d9a7e89f344acec8
@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent implements OnChanges {


  @ViewChild('treeContainer')
  treeContainerElement: ElementRef;

  @Input()
  graph: Graph;

  @Input()
  width ?= 800;

  @Input()
  height ?= 600;

  @Output()
  selectionChanged = new EventEmitter();

  margin = { top: 10, right: 10, bottom: 10, left: 50 };

  constructor(
    private graphService: GraphService,
    private D3: D3Service,
    private treeService: D3treeService
  ) { }


  ngOnChanges() {

    if (this.graph.nodes.length === 0) { return; }

    // Get tree from graph
    const treeData = this.graphService.toTree(this.graph);

    console.log(treeData);
    // Color scale
    const colorScale = d3.scaleOrdinal(d3.schemeCategory10)
      .domain(this.graph.pos_types);
    this.D3.clearCanvas(this.treeContainerElement);
    const canvas = this.D3.createCanvas(this.treeContainerElement, this.width, this.height, this.margin);
    const nodesData = this.treeService.layout(treeData, this.width, this.height * 2);

    this.treeService.createLinks(canvas, nodesData);

    this.treeService.createLinkLabels(canvas, nodesData);

    const nodes = this.treeService.createNodes(canvas, nodesData.descendants(), colorScale);

    // this.D3.createTooltips(canvas, nodes);
    this.D3.createTooltips(this.treeContainerElement.nativeElement, nodes, this.selectionChanged);




  }


}
