import { Connection } from './connection';
import { Node } from './node';
export interface Graph {
  nodes: Node[];
  connections: Connection[];
  pos_types: string[];
  root_id: string;

}
