export interface TreeNode  {
    id: any;
    text: string;
    pos: string;
    dep: string;
    children: TreeNode[];
}
