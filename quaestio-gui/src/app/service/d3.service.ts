import { Injectable, ElementRef, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import { TreeNode } from '../model/tree-node';

@Injectable({
  providedIn: 'root'
})
export class D3Service {

  constructor() { }

  clearCanvas(parent: ElementRef) {
    d3.select(parent.nativeElement).selectAll('g').remove();
  }

  createCanvas(parent: ElementRef, width, height, margin) {
    const treeContainer = d3.select(parent.nativeElement);
    const zoomableLayer = treeContainer.append('g');

    // Main canvas
    treeContainer
      .attr('width', width)
      .attr('height', height);


    // Enable zoom
    const zoom = d3.zoom().scaleExtent([-Infinity, Infinity]).on('zoom', function () {
      return zoomableLayer.attr(
        'transform', d3.event.transform
      );
    });

    treeContainer.call(zoom);

    // Create zoomable canvas
    zoomableLayer.select('svg').remove();
    const canvas = zoomableLayer.append('g')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .attr('transform',
        'translate(' + margin.left + ',' + margin.top + ')');

    return canvas;
  }

  createTooltips(parentElement, nodes, selectionChanged: EventEmitter<any>) {
    const parent = d3.select(parentElement);

    nodes
      .on('mouseover', handleMouseOver)
      .on('mouseout', handleMouseOut);

    // Create Event Handlers for mouse
    function handleMouseOver(d, i) {  // Add interactivity
      const data = [];
      // tslint:disable-next-line:forin
      for (const key in d.data) {
        if (key !== 'children') {
          data.push([key, d.data[key]]);

        }
      }
      selectionChanged.emit(data);

      // Creates tooltip
      // parent.append('foreignObject')
      //   .attr('id', 't' + d.data.i)
      //   .attr('x', 5).attr('y', 0)
      //   .attr('width', 150).attr('height', 100)

      //   .append('xhtml:body')
      //   .append('table')
      //   .selectAll('table')
      //   .data(data)
      //   .enter()
      //   .append('tr')
      //   .html((d: any) => '<td>' + d[0] + '</td>' + '<td>' + d[1] + '</td>');

    }

    function handleMouseOut(d, i) {
      // parent.selectAll('#t' + d.data.i)
      //   .remove();
      selectionChanged.emit([]);
    }
  }

}
