import { Injectable } from '@angular/core';
import * as d3 from 'd3';
import { TreeNode } from '../model/tree-node';

const BOX_MARGIN = 5;

@Injectable({
  providedIn: 'root'
})
export class D3treeService {

  constructor() { }

  layout(treeData: TreeNode, width, height) {
    const treemap = d3.tree()
      .size([height, width]);
    //  assigns the data to a hierarchy using parent-child relationships
    let nodesData = d3.hierarchy(treeData, (d: any) => {
      return d.children;
    });
    // maps the node data to the tree layout
    nodesData = treemap(nodesData);
    return nodesData;
  }


  createNodes(parent, data, colorScale) {
    const nodeClass = 'node';
    const node = parent.selectAll('.' + nodeClass)
      .data(data)
      .enter().append('g')
      .attr('class', nodeClass)
      ;

    node.append('rect');
    // adds the text to the node
    const text: any = node.append('text')
      .attr('dy', '.35em')
      .attr('x', (d: any) => {
        return 0;
      })
      .style('text-anchor', 'middle')
      .text((d: any) => d.data.text); // + '(' + d.data.pos + ')';

    text
      .each(function (t: any, i: number) {
        t.bb = (this as any).parentNode.getBBox();
      });

    // adds the circle to the node
    node.selectAll('rect')
      .style('stroke', (d: any) => 'black')
      .attr('width', (n: any) => n.bb.width + BOX_MARGIN * 2)
      .attr('height', (n: any) => n.bb.height + BOX_MARGIN)
      .attr('x', (n: any) => -n.bb.width / 2 - BOX_MARGIN)
      .attr('y', (n: any) => -n.bb.height / 2 - BOX_MARGIN / 2);


    node
      .attr('transform', (d: any) => {
        return 'translate(' + d.y + ',' + d.x + ')';
      });


    node.selectAll('rect')
      .attr('fill', (d: any) => colorScale(d.data.pos));

    return node;
  }


  createLinks(parent, nodesData) {
    return parent.selectAll('.link')
      .data(nodesData.descendants().slice(1))
      .enter()
      .append('path')
      .attr('class', 'link')
      .attr('id', (d, i) => 'treelinkpath' + i)
      .style('stroke', (d: any) => 'blue')
      .style('fill', 'none')
      .attr('d', (d: any) => {
        return 'M' + d.y + ',' + d.x
          + 'C' + (d.y + d.parent.y) / 2 + ',' + d.x
          + ' ' + (d.y + d.parent.y) / 2 + ',' + d.parent.x
          + ' ' + d.parent.y + ',' + d.parent.x;
      });

  }

  createLinkLabels(parent, nodesData) {
    // Creates link labels
    const labels = parent.selectAll('.treelinklabel')
      .data(nodesData.descendants().slice(1))
      .enter()
      .append('text')
      .style('pointer-events', 'none')
      .attr('class', 'treelinklabel', )
      .attr('id', (d, i) => 'treelinklabel' + i, )
      .attr('font-size', 15, )
      .attr('fill', '#000');


    labels.append('textPath')
      .attr('xlink:href', (d, i) => '#treelinkpath' + i)
      .style('text-anchor', 'middle')
      .style('pointer-events', 'none')
      .attr('startOffset', '50%')
      .text((d: any) => d.data.dep);

    labels
      .attr('transform', function (d) {
        const bbox = (this as any).getBBox();

        const rx = bbox.x + bbox.width / 2;
        const ry = bbox.y + bbox.height / 2;
        return 'rotate(180 ' + rx + ' ' + ry + ')';
      });
    return labels;
  }


}
