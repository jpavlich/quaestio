import { TestBed, inject } from '@angular/core/testing';

import { D3treeService } from './d3tree.service';

describe('D3treeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [D3treeService]
    });
  });

  it('should be created', inject([D3treeService], (service: D3treeService) => {
    expect(service).toBeTruthy();
  }));
});
