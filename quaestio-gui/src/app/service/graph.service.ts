import { Graph } from './../model/graph';
import { Injectable } from '@angular/core';
import { TreeNode } from '../model/tree-node';
import { Node } from '../model/node';

class GraphIndex {
  private _nodes = {};
  private _edges = {};
  private _out = {};
  private _in = {};
  private _out_edges = {};
  private _in_edges = {};

  constructor(private graph: Graph) {
    for (const node of graph.nodes) {
      this._nodes[node.id] = node;
      this._out[node.id] = [];
      this._in[node.id] = [];
      this._out_edges[node.id] = [];
      this._in_edges[node.id] = [];
    }

    for (const edge of graph.connections) {
      this._edges[edge.id] = edge;
    }

    for (const conn of graph.connections) {
      this._out[conn.source].push(this._nodes[conn.target]);
      this._in[conn.target].push(this._nodes[conn.source]);
      this._out_edges[conn.source].push(conn);
      this._in_edges[conn.target].push(conn);
    }
  }

  public in(node: Node) {
    return this._in[node.id] ? this._in[node.id] : [];
  }

  public out(node: Node) {
    return this._out[node.id] ? this._out[node.id] : [];
  }

  public in_edges(node: Node) {
    return this._in_edges[node.id] ? this._in_edges[node.id] : [];
  }

  public out_edges(node: Node) {
    return this._out_edges[node.id] ? this._out_edges[node.id] : [];
  }

  public node(id: any) {
    return this._nodes[id];
  }
}

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  constructor() { }


  private toTreeNode(graphIndex: GraphIndex, node: Node): TreeNode {
    const treeNode = { ...node, children: [] };
    for (const adj of graphIndex.in(treeNode)) {
      const child = this.toTreeNode(graphIndex, adj);
      child.dep = graphIndex.out_edges(child)[0].dep;
      treeNode.children.push(child);
    }
    return treeNode;
  }

  toTree(graph: Graph) {
    const graphIndex = new GraphIndex(graph);
    console.log(graphIndex);
    return this.toTreeNode(graphIndex, graphIndex.node(graph.root_id));
  }


}
