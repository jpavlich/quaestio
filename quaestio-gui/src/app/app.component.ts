import { Component } from '@angular/core';

import { Graph } from './model/graph';
import { NlpService } from './service/nlp.service';

declare let monaco: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  editorOptions = {
    theme: 'hc-white',
    wordWrap: 'on'
  };
  code = `1. El usuario selecciona "Modificar Perfil" en la barra de navegación del menú principal.
2. Se le mostrarán sus datos de usuario y se le permitirá modificar
algunos de ellos como son su nombre de inicio de sesión y su
contraseña. Modificará sus credenciales y pulsará "Aceptar".
3. Si los datos no son correctos, se validará el formulario y se mostrará
un mensaje de error debajo de los campos correspondientes.
4. Si los datos son correctos se modificarán en la base de datos.`;

  code2 = `Este caso de uso empieza cuando un Cliente introduce una tarjeta en el cajero.
 Pide la clave de identificación.
 Introduce la clave.
 Presenta las opciones de operaciones disponibles.
 Selecciona la operación de Reintegro.
 Pide la cantidad a retirar.
 Introduce la cantidad requerida.
 Procesa la petición y, eventualmente, da el dinero solicitado.
 Devuelve la tarjeta y genera un recibo.
 Recoge la tarjeta.
 Recoge el recibo.
 Recoge el dinero y se va`;

  result = '';
  graph: Graph = {
    nodes: [

    ],
    connections: [

    ],
    pos_types: [

    ],
    root_id: ''
  };

  conllArray = [];

  selection = [];

  constructor(private nlp: NlpService) { }


  updateAll() {
    this.nlp.process(this.code)
      .subscribe(
        (result: Graph) => {
          console.log(result);
          this.graph = result;
        },
        error => console.log(error));
  }

  updateConllView() {

    this.nlp.processConll(this.code)
      .subscribe(
        (result: any) => {
          console.log(result);
          this.conllArray = result;
        },
        error => console.log(error));
  }

  onInit(editor) {

    // editor.onDidChangeCursorPosition((e) => {
    //   console.log(JSON.stringify(e));
    // });

    // editor.onDidChangeCursorSelection((e) => {
    //   console.log(JSON.stringify(e));
    // });

    const update = () => this.updateAll();


    editor.addAction({
      // An unique identifier of the contributed action.
      id: 'process-text',

      // A label of the action that will be presented to the user.
      label: 'Process Text',

      // An optional array of keybindings for the action.
      keybindings: [
        // tslint:disable-next-line:no-bitwise
        monaco.KeyMod.CtrlCmd | monaco.KeyCode.Enter,
      ],

      // A precondition for this action.
      precondition: null,

      // A rule to evaluate on top of the precondition in order to dispatch the keybindings.
      keybindingContext: null,

      contextMenuGroupId: 'navigation',

      contextMenuOrder: 1.5,

      // Method that will be executed when the action is triggered.
      // @param editor The editor instance is passed in as a convinience
      run: function (ed) {
        update();
        return null;
      }
    });

    // this.updateAll();
  }

  updateSelection(selection) {
    this.selection = selection;
  }
}
