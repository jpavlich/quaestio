import { Connection } from './../model/connection';
import { Component, OnInit, ViewChild, ElementRef, OnChanges, Input } from '@angular/core';
import * as d3 from 'd3';
import { Graph } from '../model/graph';
import { Node } from '../model/node';

@Component({
  selector: 'app-graph-view',
  templateUrl: './graph-view.component.html',
  styleUrls: ['./graph-view.component.css']
})
export class GraphViewComponent implements OnInit, OnChanges {

  @ViewChild('graphContainer')
  graphContainerElement: ElementRef;

  @ViewChild('zoomableLayer')
  zoomableLayerElement: ElementRef;

  @Input()
  graph: Graph;

  @Input()
  width?= 800;

  @Input()
  height?= 600;
  simulation: any;


  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    // See http://bl.ocks.org/fancellu/2c782394602a93921faff74e594d1bb1
    console.log('ngOnchanges');
    const graphContainer = d3.select(this.graphContainerElement.nativeElement);
    const zoomableLayer = d3.select(this.zoomableLayerElement.nativeElement);
    graphContainer
      .attr('width', this.width)
      .attr('height', this.height);

    const zoom = d3.zoom().scaleExtent([-Infinity, Infinity]).on('zoom', function () {
      return zoomableLayer.attr(
        'transform', d3.event.transform
      );
    });

    graphContainer.call(zoom);

    const color = d3.scaleOrdinal(d3.schemeCategory10)
      .domain(this.graph.pos_types);

    // d3.selectAll('.node').remove();
    // d3.selectAll('path').remove();

    const simulation = this.initSimulation();

    graphContainer.append('defs').append('marker')
      .attr('id', 'arrowhead')
      .attr('viewBox', '-0 -5 10 10')
      .attr('refX', 25)
      .attr('refY', 0)
      .attr('orient', 'auto')
      .attr('markerWidth', 13)
      .attr('markerHeight', 13)
      .attr('xoverflow', 'visible')
      .append('svg:path')
      .attr('d', 'M 0,-5 L 10,0 L 0,5')
      .attr('fill', '#777')
      .style('stroke', 'none');

    let connections = graphContainer
      .select('#connections')
      .selectAll('path')
      .data(this.graph.connections);

    connections.exit().remove();

    connections = connections
      .enter()
      .append('path')
      .attr('marker-end', 'url(#arrowhead)')
      .attr('class', 'edgepath')
      .attr('stroke', 'black')
      .attr('id', (d, i) => 'edgepath' + i)
      .style('pointer-events', 'none');

    let labels = graphContainer
      .select('#labels')
      .selectAll('text')
      .data(this.graph.connections);

    labels.exit().remove();

    labels = labels.enter()
      .append('text')
      .style('pointer-events', 'none')
      .attr('class', 'edgelabel', )
      .attr('id', (d, i) => 'edgelabel' + i, )
      .attr('font-size', 15, )
      .attr('fill', '#000');


    labels.append('textPath')
      .attr('xlink:href', (d, i) => '#edgepath' + i)
      .style('text-anchor', 'middle')
      .style('pointer-events', 'none')
      .attr('startOffset', '50%')
      .text((d: any) => d.dep);


    // Sets data
    let nodes = graphContainer
      .select('#nodes')
      .selectAll('g')
      .data(this.graph.nodes);

    // Remove remaining nodes
    nodes.exit().remove();

    // Add new nodes
    nodes = nodes
      .enter()
      .append('g')
      .call(d3.drag<SVGGElement, Node>()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended));

    nodes
      .append('circle')
      .attr('class', 'node')
      .attr('r', 20)
      .attr('fill', (n: Node) => color(n.pos));

    nodes
      .append('text')
      .attr('class', 'node')
      .text((n: Node) => n.text)
      .attr('text-anchor', 'middle')
      .style('pointer-events', 'none')
      .call(d3.drag<SVGGElement, Node>()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended));



    this.updateSimulation(ticked);

    function ticked() {
      const k = 20 * simulation.alpha();

      // Push sources up and targets down to form a weak tree.
      connections
        .attr('d', function (d) {
          return 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y;
        })
        .attr('x1', (d: any) => d.source.x)
        .attr('y1', (d: any) => d.source.y)
        .attr('x2', (d: any) => d.target.x)
        .attr('y2', (d: any) => d.target.y);

      labels.attr('transform', function (d) {
        if (d.target.x < d.source.x) {
          const bbox = (this as any).getBBox();

          const rx = bbox.x + bbox.width / 2;
          const ry = bbox.y + bbox.height / 2;
          return 'rotate(180 ' + rx + ' ' + ry + ')';
        } else {
          return 'rotate(0)';
        }
      });

      nodes
        .attr('transform', (d: any) => 'translate(' + d.x + ',' + d.y + ')');
    }

    function dragstarted(d) {
      if (!d3.event.active) { simulation.alphaTarget(0.3).restart(); }
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) { simulation.alphaTarget(0); }
      d.fx = null;
      d.fy = null;
    }

  }




  private updateSimulation(ticked: () => void) {
    this.simulation
      .nodes(this.graph.nodes)
      .on('tick', ticked);
    this.simulation.force('link')
      .links(this.graph.connections)
      .distance(100);
    this.simulation.force('charge')
      .strength(-100);
    this.simulation.alpha(1).restart();
  }

  private initSimulation() {
    this.simulation = d3.forceSimulation();
    this.simulation
      .force('link', d3.forceLink().id(function (n: Node) { return n.id; }))
      .force('charge', d3.forceManyBody())
      .force('center', d3.forceCenter(this.width / 2, this.height / 2));
    return this.simulation;
  }
}
